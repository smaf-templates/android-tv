package com.smafsdk.common;

import android.app.Activity;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.util.DisplayMetrics;
import android.view.Display;

import com.smafsdk.R;

/**
 * Created by Smaf.tv on 6/9/15.
 */
public abstract class AbstractActivity extends Activity
{
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
    }

    /**
     * Initialises the default DISPLAY
     */
    protected int getInitialScale()
    {
        DisplayMetrics displaymetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
        int screenHeight = displaymetrics.heightPixels;
        return (int) Math.round((screenHeight/(1.0*getResources().getInteger(R.integer.resolution))) * 100.0);
    }
}
