package com.smafsdk.model;

import java.io.Serializable;

/**
 * Created by Smaf.tv on 6/9/15.
 */
public class DeviceInfo implements Serializable {

    /**
     * The current SDK
     */
    private int sdk;

    /**
     * The name of the industrial design.
     */
    private String device;

    /**
     * The end-user-visible name for the end product.
     */
    private String model;

    /**
     * The name of the overall product.
     */
    private String product;

    /**
     * The manufacturer of the product/hardware.
     */
    private String manufacturer;

    /**
     * The country as found by current locale.
     */
    private String country;

    /**
     * Current locale as string.
     */
    private String locale;

    /**
     * Locale's language.
     */
    private String language;

    public DeviceInfo()
    {
    }

    // GETTER AND SETTERS //

    public int getSdk() {
        return sdk;
    }

    public void setSdk(int sdk) {
        this.sdk = sdk;
    }

    public String getDevice() {
        return device;
    }

    public void setDevice(String device) {
        this.device = device;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getProduct() {
        return product;
    }

    public void setProduct(String product) {
        this.product = product;
    }

    public String getManufacturer() {
        return manufacturer;
    }

    public void setManufacturer(String manufacturer) {
        this.manufacturer = manufacturer;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getLocale() {
        return locale;
    }

    public void setLocale(String locale) {
        this.locale = locale;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }
}
